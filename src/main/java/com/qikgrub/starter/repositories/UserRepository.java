package com.qikgrub.starter.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.qikgrub.starter.entities.User;

public interface UserRepository extends CrudRepository<User, Long>{

	public Optional<User> findByEmail(String email);

}
