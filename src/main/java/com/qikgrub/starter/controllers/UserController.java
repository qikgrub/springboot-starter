package com.qikgrub.starter.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.qikgrub.starter.services.impl.UserServiceImpl;
import com.qikgrub.starter.web.models.WebUser;

@RestController
@RequestMapping("/api")
public class UserController {

	@Autowired
	UserServiceImpl userServiceImpl;

	@PostMapping("/add/user")
	public ResponseEntity<Object> addUser(@Valid @RequestBody WebUser webUser) {
		return userServiceImpl.addUser(webUser);
	}

	@DeleteMapping("/delete/user/{userId}")
	public ResponseEntity<Object> deleteIndividual(@PathVariable Long userId) {
		return userServiceImpl.deleteIndividual(userId);
	}

	@GetMapping("/get/users")
	public ResponseEntity<Object> getUsers() {
		return userServiceImpl.getUsers();
	}

	@GetMapping("/get/user/{userId}")
	public ResponseEntity<Object> getUser(@PathVariable Long userId) {
		return userServiceImpl.getUser(userId);
	}

	@PostMapping("/update/user")
	public ResponseEntity<Object> updateUser(@Valid @RequestBody WebUser webUser) {
		return userServiceImpl.updateUser(webUser);
	}
}
