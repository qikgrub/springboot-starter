package com.qikgrub.starter.mappers;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.qikgrub.starter.entities.User;
import com.qikgrub.starter.repositories.UserRepository;
import com.qikgrub.starter.web.models.WebUser;

@Component
public class UserMapper {
	
	@Autowired
    private ModelMapper modelMapper;
	
	@Autowired
	UserRepository userRepository;
	
	public User userMapper(WebUser webUser) {
		User user = new User();
		if(webUser.getId() != null) {
			user = userRepository.findById(webUser.getId()).get();
		}
		user.setFirstName(webUser.getFirstName());
		user.setLastName(webUser.getLastName());
		user.setEmail(webUser.getEmail());
		user.setPhone(webUser.getPhone());
		return user;
	}

	public WebUser webUserMapper(User user) {
		WebUser webUser = modelMapper.map(user, WebUser.class);
	    return webUser;
	}

	public List<WebUser> webUserMapper(List<User> userList) {
		List<WebUser> webUserList = new ArrayList<WebUser>();
		for (User user : userList) {
			webUserList.add(webUserMapper(user));
		}
		return webUserList;
	}

}
