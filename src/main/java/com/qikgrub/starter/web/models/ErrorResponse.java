package com.qikgrub.starter.web.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class ErrorResponse {
	
    @NonNull
    private Integer status;
    
    @NonNull
    private String error;
    
    @NonNull
    private String error_description;
}
