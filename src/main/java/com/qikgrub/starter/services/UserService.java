package com.qikgrub.starter.services;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;

import com.qikgrub.starter.web.models.WebUser;

public interface UserService {

	public ResponseEntity<Object> addUser(WebUser webUser);

	public ResponseEntity<Object> deleteIndividual(Long userId);

	public ResponseEntity<Object> getUsers();

	public ResponseEntity<Object> updateUser(@Valid WebUser webUser);

	public ResponseEntity<Object> getUser(Long userId);

}
