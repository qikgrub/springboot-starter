package com.qikgrub.starter.services.impl;

import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.qikgrub.starter.entities.User;
import com.qikgrub.starter.mappers.UserMapper;
import com.qikgrub.starter.repositories.UserRepository;
import com.qikgrub.starter.services.UserService;
import com.qikgrub.starter.web.models.ErrorResponse;
import com.qikgrub.starter.web.models.Response;
import com.qikgrub.starter.web.models.WebUser;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	UserRepository userRepository;

	@Autowired
	UserMapper userMapper;
	
	
	@Override
	public ResponseEntity<Object> addUser(WebUser webUser) {
		try {
			User existingUser = userRepository.findByEmail(webUser.getEmail()).orElse(null);
			if (existingUser == null) {
				User user = userMapper.userMapper(webUser);
				userRepository.save(user);
				return new ResponseEntity<>(new Response("User Added Successfully"), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new ErrorResponse(400, "error", "Email Address Already Exists"), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception ex) {
			return new ResponseEntity<>(new ErrorResponse(500, "internal_server_error", "Database Error"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public ResponseEntity<Object> deleteIndividual(Long userId) {
		try {
			User user = userRepository.findById(userId).orElse(null);
			if (user != null) {
				userRepository.delete(user);
				return new ResponseEntity<>(HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new ErrorResponse(400, "bad_request", "Invalid User Id"), HttpStatus.BAD_REQUEST);
			}
		} catch (Exception ex) {
			return new ResponseEntity<>(new ErrorResponse(500, "internal_server_error", "Database Error"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public ResponseEntity<Object> getUsers() {
		try {
			List<User> users = (List<User>) userRepository.findAll();
			return new ResponseEntity<>(userMapper.webUserMapper(users), HttpStatus.OK);
		} catch (Exception ex) {
			return new ResponseEntity<>(new ErrorResponse(500, "internal_server_error", "Database Error"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public ResponseEntity<Object> getUser(Long userId) {
		try {
			User user = userRepository.findById(userId).orElse(null);
			if(user == null) {
				return new ResponseEntity<>(new ErrorResponse(400, "error", "No User Found"), HttpStatus.INTERNAL_SERVER_ERROR);
			}
			return new ResponseEntity<>(userMapper.webUserMapper(user), HttpStatus.OK);
		} catch (Exception ex) {
			return new ResponseEntity<>(new ErrorResponse(500, "internal_server_error", "Database Error"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public ResponseEntity<Object> updateUser(@Valid WebUser webUser) {
		try {
			if (webUser.getId() != null) {
				User user = userMapper.userMapper(webUser);
				userRepository.save(user);
				return new ResponseEntity<>(new Response("User u Successfully"), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(new ErrorResponse(400, "error", "Email Address Already Exists"), HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception ex) {
			return new ResponseEntity<>(new ErrorResponse(500, "internal_server_error", "Database Error"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
