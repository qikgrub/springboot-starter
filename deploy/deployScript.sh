#!/bin/bash

if [[ $# -ne 3 ]]; then
	echo "Error 001: Syntax: deployScript.sh <ENV> <HOST> <USER>";
	exit 1;
fi

ENV=$1;
HOST=$2;
USER=$3;
DESTINATION="/home/${USER}/qikgrub/backend"

echo "Copying Files to Destination"
scp target/*.jar $USER@$HOST:$DESTINATION
if [[ $? -ne 0 ]]; then
    echo "Error 002: issue with transferring files - $files";
    exit 2;
fi

ssh $USER@$HOST << 'EOF'
echo $DESTINATION
pwd
cd /home/${USER}/qikgrub/backend
pwd
export SPRING_PROFILES_ACTIVE=$ENV
echo "Killing Previous Application Version"
if test -z "$(cat run.pid)"
then
      echo "pid is not present"
else
    sudo kill $(cat run.pid)
fi
if [[ $? -ne 0 ]]; then
    echo "Error 003: No process found to kill with pid - $(cat run.pid)";
fi
sleep 5
echo "Running Spring Boot Application"
nohup java -jar *.jar >> qikgrub.logs 2>&1 & echo $! > run.pid
EOF

echo "Deployment Completed!"