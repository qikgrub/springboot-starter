CREATE TABLE `user` (
    `id` int(11) NOT NULL,
    `first_name` varchar(255) NOT NULL,
    `middle_name` varchar(255) NULL,
    `last_name` varchar(255) NOT NULL,
    `email` varchar(255) NOT NULL,
    `phone` varchar(255) NOT NULL
);

ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE `u1_user` (`email`),
  ADD INDEX `i1_user` (`last_name`,`first_name`),
  ADD INDEX `i2_user` (`email`);
  
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `user`
  AUTO_INCREMENT = 10001;

INSERT INTO `user` (`id`,`first_name`, `middle_name`, `last_name`, `email`, `phone`) 
VALUES (10000, 'Koushik', NULL, 'M L N', 'koushik@itversity.in', '+91-897-870-9714');

INSERT INTO `user` (`id`,`first_name`, `middle_name`, `last_name`, `email`, `phone`) 
VALUES (10001, 'Nikhil', NULL, 'Verma', 'nikhil@itversity.in', '+91-797-907-8862');